[LAB8 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab8/setup.md)

## Part C_1 - Part C Lab -- Step-by-step guide
**Synopsis:** In this part, the Part C lab instructions are written out.

## STEPS TO MODIFY in ```main.py```

### Step C8: Copy Lab6 Code to Lab8 Dir

First, copy all of the code from Lab6 into Lab8.  Then make the following minor
changes to your ```main.py```.  Later, we will change the ```localBroker``` variable
to point to whatever platform is hosting Mosquitto the MQTT broker.

```sh
pi$ cp ../Lab6/webapp .
```

### STEP C9: Initial Import structure

```python
#!/usr/bin/python
import time
import datetime
from sense import PiSenseHat
import paho.mqtt.client as paho
from flask import *
```

### STEP C10: MQTT Callbacks

```python
# ============================= MQTT Callbacks ================================
# The callback for when a CONNACK message is received from the broker.
def on_connect(client, userdata, flags, rc):
    print("CONNACK received with code %d." % (rc))

# The callback for when a PUBLISH message is received from the broker.
def on_message(client, userdata, msg):
    print (string.split(msg.payload))
# ============================= MQTT Callbacks ================================
```

### STEP C11: LOGIN CONSTANTS and SETUP TO PUBLISH

```python
# MQTT Configuration for local network
localBroker = "localhost"	# Local MQTT broker
localPort   = 1883			# Local MQTT port
localUser   = "pi"			# Local MQTT user
localPass = "raspberry"		# Local MQTT password
localTopic = "iot/sensor"	# Local MQTT topic to monitor
localTimeOut = 120			# Local MQTT session timeout

# Setup to Publish Sensor Data
mqttc = paho.Client()
mqttc.on_connect = on_connect
mqttc.on_message = on_message
mqttc.connect(localBroker, localPort, localTimeOut)
```

### Step C12: Modify our WebApp main.py SSE route and method

Add mqtt publish call in the SSE route loop:

```python
# =========================== Endpoint: /myData ===============================
# read the sensor values by GET method from curl for example
# curl http://iot8e3c:5000/myData
# -----------------------------------------------------------------------------
@app.route('/myData')
def myData():
    def get_values():
        while True:
            # return the yield results on each loop, but never exits while loop
            data_payload = get_sensor_values()
            yield('data: {0}\n\n'.format(data_payload))
            print("MQTT Topic:"+localTopic, data_payload)
            mqttc.publish(localTopic,data_payload)
            time.sleep(2.0)
    return Response(get_values(), mimetype='text/event-stream')
# ============================== API Routes ===================================
```

### Step C13: Modify Sense.py to include LED display

Add the following two functions from line 24 on the sense.py from Lab6

```python
    # pixel display
    def set_pixel(self,x,y,color):
        # red = (255, 0, 0)
        # green = (0, 255, 0)
        # blue = (0, 0, 255)
        self.sense.set_pixel(x, y, color)

    # clear pixel display
    def clear_display(self):
        self.sense.clear()
```

### Step C14: Connect Sense Hat LED Matrix display to SSE

```python
TBD
```

[PART D](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab8/PartD.md) Test Multiple Pub/Sub Connections

[LAB8 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab8/setup.md)
