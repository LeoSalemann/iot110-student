[LAB4 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab4/setup.md)

## Perform BMP280 Unit Tests

## Objectives
* Run unit test for ```bmp280.py``` stand alone
* Run unit test for abstract test methods of multiple BMP280 sensors.

### Step D1 – set up Unit Tests for them to become executables
```sh
pi$ chmod +x bmp280.py
pi$ chmod +x bmp280_test.py
pi$ chmod +x bmp280_test2.py
```
### Step D2 – Test the methods in bmp280.py
```sh
pi$ ./bmp280.py
    Chip ID : 88
    Version : 1
Temperature : 23.61 C
   Pressure : 1009.74418394 hPa
```

### Step D3 – One sensor test in bmp280_test.py
```sh
pi$ ./bmp280_test.py
============================== SENSOR 1 ==============================
Chip ADDR : 0x76
  Chip ID : 88
  Version : 1
Temperature : 23.61 C
 Pressure : 1009.71752441 hPa
{'addr': 118,
'chip': <bmp280.PiBMP280 object at 0x76a887b0>,
'data': {'chip_id': 88,
        'chip_version': 1,
        'pressure': {'reading': 1009.7175244112908, 'units': 'hPa'},
        'temperature': {'reading': 23.61, 'units': 'C'}},
'name': 'bmp280'}
======================================================================
```

### Step D4 – Two sensor test in bmp280_test2.py
```sh
============================== SENSOR 1 ==============================
Chip ADDR : 0x76
  Chip ID : 88
  Version : 1
Temperature : 23.61 C
 Pressure : 1009.71752441 hPa
{'addr': 118,
'chip': <bmp280.PiBMP280 object at 0x76a887b0>,
'data': {'chip_id': 88,
        'chip_version': 1,
        'pressure': {'reading': 1009.7175244112908, 'units': 'hPa'},
        'temperature': {'reading': 23.61, 'units': 'C'}},
'name': 'bmp280'}
============================== SENSOR 2 ==============================
Chip ADDR : 0x77
  Chip ID : 88
  Version : 1
Temperature : 23.59 C
 Pressure : 1009.7345 hPa
{'addr': 118,
'chip': <bmp280.PiBMP280 object at 0x76a887b0>,
'data': {'chip_id': 88,
        'chip_version': 1,
        'pressure': {'reading': 1009.7345, 'units': 'hPa'},
        'temperature': {'reading': 23.59, 'units': 'C'}},
'name': 'bmp280'}
======================================================================
```

[PART C](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab4/PartC.md) Back to Part C

[LAB4 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab4/setup.md)
