[LAB6 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab6/setup.md)

## Part D - Finally!  The Web appeared
**Synopsis:** Finally we will tie all of this together and have all of our sensor
values both plotable and graphed on our Morris JS Chart.

## Objectives
* Test that we can receive data from the Driver and Main
* Add code to display Environmental Data in a Table in real-time
* Add code to display Inertial Data in a Table in real-time
* Add code to display Environmental Data in a in a Chart Form
* Add code to display Inertial Data in a Chart Form

### Step D1: Let's test the incoming data
```Javascript
$(document).ready(function() {

  // the key event receiver function
  iotSource.onmessage = function(e) {
    // parse sse received data
    parsed_json_data = JSON.parse(e.data);

    // For now just log to console
    console.log(parsed_json_data);
  }
```    

### Step D2: Store SSE Data

```Javascript
$(document).ready(function() {

  // Add variable to set maximum number of data point saved and table length
  var max_data_saved = 20;
  //Add variable to hold data between receive data events
  var sse_sensor_data = [];

  iotSource.onmessage = function(e) {
    parsed_json_data = JSON.parse(e.data);
    console.log(parsed_json_data);
    parsed_json_data['meas_time'] = new Date(parsed_json_data['meas_time']);


// Push new data to front of saved data list
    sse_sensor_data.unshift(parsed_json_data);

    // Then remove oldest data up to maximum data saved
    while (sse_sensor_data.length > max_data_saved) { sse_sensor_data.pop(); }

  }
```    

### Step D3: Update Data Table

```Javascript
$(document).ready(function() {

  var max_data_saved = 20;
  var sse_sensor_data = [];

  iotSource.onmessage = function(e) {
    parsed_json_data = JSON.parse(e.data);
    console.log(parsed_json_data);
    parsed_json_data['meas_time'] = new Date(parsed_json_data['meas_time']);


    sse_sensor_data.unshift(parsed_json_data);
    while (sse_sensor_data.length > max_data_saved) { sse_sensor_data.pop(); }

    // Call the function that updates the environmental sensors table
    updateEnvironmentalTable();
  }

    function updateEnvironmentalTable() {
        // Iterate over each environmental parameter row
        $('tr.env-param-row').each(function(i) {
            // Start new string of output html, add in measurement time
            var new_html = '<td>' + sse_sensor_data[i].meas_time.toLocaleString() + '</td>';
            // And add in each specific env sensor data value
            new_html += '<td>' + sse_sensor_data[i]['environmental']['temperature'].value.toFixed(2) + '</td>';
            new_html += '<td>' + sse_sensor_data[i]['environmental']['pressure'].value.toFixed(2) + '</td>';
            new_html += '<td>' + sse_sensor_data[i]['environmental']['humidity'].value.toFixed(2) + '</td>';

            // Then replace the current html with this newly created table row
            $(this).html(new_html);
        });
    }

```

### Step D4: Update Enviromental Data Chart

```Javascript

    // In SSE OnMessage Event Handler add function call to update_env_chart()

    // ============================== ENV CHART ================================
    // initialize the accel chart structure
    var env_chart = new Morris.Line({
        element: 'env-chart',
        data: '',
        xkey: 'time',
        ykeys: ['humidity', 'temp'],
        labels: ['%RH', '&degC']
    });

    // build the environmental chart data array for MorrisJS structure
    function update_env_chart(data) {
        var chart_data = [];
        // Create chart data object to pass to morris chart object
        sse_sensor_data.forEach(function(d) {
            env_record = {
                'time': d['meas_time'].getTime(),
                'humidity': d['environmental']['humidity'].value,
                'temp': d['environmental']['temperature'].value
            };
            chart_data.push(env_record);
        });
        env_chart.setData(chart_data);
    };
    // ============================== ENV CHART ================================

```

### Step D5: Update web page for other Sense Hat data

* Add table and chart for Inertial sensor
* Add data and indicators for joystick
* Add data and controls for 8x8 LED display (optional)


![Inertial  Table Data](https://gitlab.com/iot110/iot110-student/raw/master/Resources/Images/InertialTable.png)

![Inertial Chart Data](https://gitlab.com/iot110/iot110-student/raw/master/Resources/Images/InertialChart.png)

[PART C](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab6/PartC.md) SenseHat Driver and Main WebApp Python Code

[LAB6 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab6/setup.md)
